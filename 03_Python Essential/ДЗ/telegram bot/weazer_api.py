from config import WEATHER_APP_ID
import aiohttp


async def get_weather(city):
    url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={WEATHER_APP_ID}'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            data = await response.json()
            code = data["cod"]
            if code == "404":
                return 'City not found!'

            city = data["name"]
            try:
                main = data["weather"][0]["main"]
            except IndexError:
                main = ''
            temp = int(data["main"]["temp"]) - 273
            wind = data["wind"]["speed"]
            humidity = data["main"]["humidity"]
            info = f'{city}\n{main}\nTemperature: {temp}\nWind: {wind}\nH: {humidity}%'
            return info